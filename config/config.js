var dbServer
var dbName

if (process.env.NODE_ENV === 'production') {
  dbServer = 'https://prod.la-recolte.net'
  dbName = 'larecolte_production'
} else if (process.env.NODE_ENV === 'uat') {
  dbServer = 'https://temp.la-recolte.net'
  dbName = 'larecolte_production'
} else {
  dbServer = 'https://temp.la-recolte.net'
  dbName = 'larecolte_production'
}

module.exports = {
  TOKEN_SECRET: process.env.TOKEN_SECRET || 'UMMMM',
  PORT: 3000,
  DB_SERVER: dbServer,
  DB_NAME: dbName,
  DB_USERNAME: 'inventaire@la-recolte.net',
  DB_PASSWORD: 'inventaire01',
  DB_OPTIONS: {'context': {'lang': 'fr_FR'}}
}
