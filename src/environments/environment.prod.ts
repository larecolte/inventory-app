export const environment = {
  production: true,
  inventoryApiUrl: 'api',
  loginUrl: 'auth'
};
