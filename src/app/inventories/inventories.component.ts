import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { InventoryService } from '../inventory.service';

@Component({
  selector: 'app-inventories',
  templateUrl: './inventories.component.html',
  styleUrls: ['./inventories.component.scss']
})
export class InventoriesComponent implements OnInit {

  inventories: any

  constructor(
    private inventoryService: InventoryService,
    private router: Router) { }

  ngOnInit() {
    this.inventoryService.getInventoriesByStatus('confirm').subscribe((inventories) => {
      this.inventories = inventories
    })
  }

}
