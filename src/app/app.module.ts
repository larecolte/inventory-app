import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule }    from '@angular/http';
import { HttpClientModule, HTTP_INTERCEPTORS  }    from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { StorageServiceModule } from 'ngx-webstorage-service';
import { JwtHelperService  } from '@auth0/angular-jwt';

import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { ToolbarComponent } from './toolbar/toolbar.component';
import { ItemListComponent } from './item-list/item-list.component';
import { SearchComponent } from './search/search.component';

import { InventoryService } from './inventory.service';
import { BarcodeReaderService } from './barcode-reader.service';
import { AuthService } from './auth.service';
import { LoginComponent } from './login/login.component';
import { InventoriesComponent } from './inventories/inventories.component';
import { AuthGuardService } from './auth-guard.service';
import { AuthInterceptorService } from './auth-interceptor.service';
import { ScrollToDirective } from './scroll-to.directive';


@NgModule({
  declarations: [
    AppComponent,
    ToolbarComponent,
    ItemListComponent,
    SearchComponent,
    LoginComponent,
    InventoriesComponent,
    ScrollToDirective
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [InventoryService, BarcodeReaderService, AuthService, AuthGuardService
    ,{
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptorService,
      multi: true
    }
    ],
  bootstrap: [AppComponent]
})
export class AppModule { }
