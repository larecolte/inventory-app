import { Component, OnInit } from '@angular/core';

import { AuthService } from '../auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  user = { username: '', password: ''}
  isUnauthorized = false

  constructor(
    private authService: AuthService) { }


  login(): void {
    this.authService.login(this.user).subscribe(
     (res) => {
      console.log("identified")
     },
     error => this.isUnauthorized = true)
  }

  toggleIsUnauthorized(): void {
    this.isUnauthorized = !this.isUnauthorized
  }

  ngOnInit() {
  }

}
