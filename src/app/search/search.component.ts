import { Component, OnInit } from '@angular/core';
import { InventoryService } from '../inventory.service';
import { BarcodeReaderService } from '../barcode-reader.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {

  barcode: number

  constructor(
    private inventoryService: InventoryService,
    private barcodeReaderService: BarcodeReaderService
    ) { }

  addItem(): void {
    this.barcodeReaderService.addBarcode(this.barcode)
  }

  ngOnInit() {

    this.barcodeReaderService.scannedBarcode.subscribe((barcode) => {
       this.barcode = Number(barcode)
    })
  }

}
