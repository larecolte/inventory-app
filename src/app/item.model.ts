import { Produce } from "./produce.model"

export class Item {
  id?: number;
  produce: Produce;
  actualQuantity?: number;
  theoreticalQuantity?: number;
}
