import { Directive, ElementRef, AfterViewInit, Input } from '@angular/core';
import { InventoryService } from './inventory.service';

@Directive({
  selector: '[appScrollTo]'
})
export class ScrollToDirective implements AfterViewInit {

  constructor(
    private el: ElementRef,
    private inventoryService: InventoryService) { 
  }

  @Input('appScrollTo') id: number;

  ngAfterViewInit() {
    this.inventoryService.lastAdded.subscribe((item) => {
      if (this.id === item.produce.id) {
        console.log("should be scrolling")
        let el = this.el.nativeElement
        el.scrollIntoView({behavior:"smooth"})        
      }
    })
  }

}
