import { TestBed, inject } from '@angular/core/testing';

import { BarcodeReaderService } from './barcode-reader.service';

describe('BarcodeReaderService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ScannerService]
    });
  });

  it('should be created', inject([BarcodeReaderService], (service: BarcodeReaderService) => {
    expect(service).toBeTruthy();
  }));


  //scannedBarcode should be a BehaviorSubject on the scanner service. Component can subscribe to it to get newly scanned barcodes.
  it('should listen to scanner input and return result', inject([BarcodeReaderService], (service: BarcodeReaderService) => {
    service.scannedBarcode.subscribe((result) => {
      expect(result).toEqual(jasmine.any(Number));
     })
  }));

  //readBarcode should take a barcode as input and return a product, along with a quantity read from the barcode
  it('should take a barcode as input and return a product and a quantity', inject([BarcodeReaderService], (service: BarcodeReaderService) => {
    let barcode = 22
    let expectedResult = {item: {"id":2,"barcode": 3770010589328,"name":"Kombucha Citron","theoreticalQuantity":2,"pricePerUnit":5.5}, quantity: 1}
    service.readBarcode().subscribe((result) => {
      expect(result).toEqual(expectedResult);
     })
  }));

  //rule W
  //readBarcode should be able to read a quantity from the barcode
  it('should take a barcode as input and return a product and a quantity', inject([BarcodeReaderService], (service: BarcodeReaderService) => {
    let barcode = 2100004002500
    let expectedResult = {item: {"id":4,"barcode": 2100004000001,"name":"Aged Gouda","theoreticalQuantity":0.5,"pricePerUnit":5.0}, quantity: 0.25}
    service.readBarcode(barcode).subscribe((result) => {
      expect(result).toEqual(expectedResult);
     })
  }));

  //rule P
  //readBarcode should be able to read and deduce a quantity from a price extracted from the barcode (weight=readPrice/pricePerUnit)
  it('should take a barcode as input and return a product and a quantity', inject([BarcodeReaderService], (service: BarcodeReaderService) => {
    let barcode = 2300005002503
    let expectedResult = {item: {"id":5,"barcode": 2300005000001,"name":"Smoked Salmon","theoreticalQuantity":2.5,"pricePerUnit":15} , quantity: 0.167}
    service.readBarcode(barcode).subscribe((result) => {
      expect(result).toEqual(expectedResult);
     })
  }));

  //rule PF
  //readBarcode should be able to read and deduce a quantity from a price in French Franc extracted from the barcode (weight=readPrice/pricePerUnit/6.55957)
  //Quantity should be rounded to 3 decimals
  it('should take a barcode as input and return a product and a quantity', inject([BarcodeReaderService], (service: BarcodeReaderService) => {
    let barcode = 2200001012127
    let expectedResult = {item: {"id":1,"barcode": 2200001000001,"name":"Apples","theoreticalQuantity":2.5,"pricePerUnit":5.0}, quantity: 0.370}
    service.readBarcode(barcode).subscribe((result) => {
      expect(result).toEqual(expectedResult);
     })
  }));

});
