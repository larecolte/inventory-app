import { Item } from "./item.model"

export class Inventory {
  id?: number;
  name?: string;
  items: Item[];
  location?: number;
  status?: string;
}
