export class Produce {
  id: number;
  barcode?: string;
  name: string;
  pricePerUnit?: number;
  quantityInStock?: number;
}
