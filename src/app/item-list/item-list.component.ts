import { Component, OnInit, HostListener} from '@angular/core';
import { InventoryService } from '../inventory.service';
import { BarcodeReaderService } from '../barcode-reader.service';
import { Item } from '../item.model'
import { Inventory } from '../inventory.model'
import { ActivatedRoute, Router } from '@angular/router';


@Component({
  selector: 'app-item-list',
  templateUrl: './item-list.component.html',
  styleUrls: ['./item-list.component.scss']
})
export class ItemListComponent implements OnInit {

  items: Item[]
  inventory: Inventory
  lastAdded: Item
  barcode: number
  inventoryId: number
  quantityToAdd = 10
  isOverriding = false
  isScanning = false
  cantFindProduce = false
  errorMessage: string

  constructor(
    private inventoryService: InventoryService,
    private barcodeReaderService: BarcodeReaderService,
    private route: ActivatedRoute,
    private router: Router
    ) { }

  @HostListener('window:keydown', ['$event'])
  keyEvent(event: KeyboardEvent) {
    console.log("keydown")
    console.log(event)
    this.barcodeReaderService.addKey(event.key, event.timeStamp)
  }
  
  isSelected(item): boolean {
    return item.produce.id === this.lastAdded.produce.id
  }

  toggleIsOverriding(): void {
    this.isOverriding = !this.isOverriding
    this.inventoryService.saveLocal(this.inventory)
  }

  toggleIsScanning(): void {
    this.isScanning = !this.isScanning
    this.inventoryService.saveLocal(this.inventory)
  }

  toggleCantFindProduce(): void {
    this.cantFindProduce = !this.cantFindProduce
  }

  incrementQuantity(increment): void {
    this.lastAdded.actualQuantity += increment
  }

  saveOverride () {
    //this.quantityToAdd = 0
    this.toggleIsOverriding()
  }

  save () {
    this.inventoryService.save(this.inventory).subscribe(res => {
      console.log("inventory saved")
      console.log(res)
    }, error => this.errorMessage = <any>error)
    this.router.navigate(['inventories'])
  }

  overidde (item) {
    this.toggleIsOverriding()
    this.lastAdded = item
  }

  onEnterIncrement(event) {
    if(event.keyCode == 13) {
      this.toggleIsOverriding()
    }
  }

  onEnterAdd(event) {
    if(event.keyCode == 13) {
      this.incrementQuantity(this.quantityToAdd)
      //this.quantityToAdd = 0
      this.toggleIsOverriding()
    }
  }

  onEnterAddBarcode(event) {
    if(event.keyCode == 13) {
      this.barcodeReaderService.addBarcode(this.barcode)
      this.barcode = undefined
    }
  }

  reset () {
    this.inventoryService.reset(this.inventory.id).subscribe(res => console.log("inventory reset"))
  }

  ngOnInit() {
    this.inventoryId = this.route.snapshot.params.inventory;
    this.inventoryService.getInventory(this.inventoryId).subscribe(res => {
      this.inventoryService.inventory.subscribe((inventory) => {
        this.inventory = inventory
      })
    })
    this.inventoryService.lastAdded.subscribe((item) => {
      this.lastAdded = item
     })
    this.barcodeReaderService.isScanning.subscribe((isScanning) => {
       this.isScanning = isScanning
     })
    this.barcodeReaderService.cantFindProduce.subscribe((cantFindProduce) => {
       this.cantFindProduce = cantFindProduce
     })
  }

}
