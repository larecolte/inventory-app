import { async, ComponentFixture, TestBed, inject } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { HttpClientModule }    from '@angular/common/http';
import {By} from "@angular/platform-browser";
import {DebugElement} from "@angular/core";

import { ItemListComponent } from './item-list.component';
import { SearchComponent } from '../search/search.component';

import { InventoryService } from '../inventory.service';
import { Item } from '../item.model';

describe('ItemListComponent', () => {
  let component: ItemListComponent;
  let fixture: ComponentFixture<ItemListComponent>;
  let el: HTMLElement;
  let service: InventoryService

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        FormsModule,
        HttpClientModule
      ],
      declarations: [ ItemListComponent, SearchComponent ],
      providers: [InventoryService]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemListComponent);
    service = TestBed.get(InventoryService);
    component = fixture.componentInstance;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should show a list of items', () => {
    let item : Item = {"id": 3770010589328,"name":"Kombucha Citron","theoreticalQuantity":2,"quantity":0}
    service.addItem(3770010589328).subscribe((result) => {
      el = fixture.debugElement.query(By.css('div')).nativeElement
      expect(el.textContent.trim()).toContain(item.name);
    })
    //fixture.detectChanges();
    //expect(el.textContent.trim()).toContain({"id": 3770010589328,"name":"Kombucha Citron","theoreticalQuantity":2});
  });
  
});
