import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { AsyncSubject } from 'rxjs/AsyncSubject';
import { map, tap, mergeMap, flatMap } from 'rxjs/operators';

import { environment } from './../environments/environment';
import { InventoryService } from './inventory.service';
import { BarcodeRule } from './barcode-rule.model';
import { Produce } from './produce.model';
import { Item } from './item.model';

import 'rxjs/add/operator/toPromise';

@Injectable()
export class BarcodeReaderService {

  private inventoryApiUrl = environment.inventoryApiUrl;
  private barcodeRules = new AsyncSubject<BarcodeRule[]>()
  private produces = new AsyncSubject<Produce[]>()
  private produceMap = new AsyncSubject<object>();
  public barcode: number
  public scannedBarcode = new BehaviorSubject<string>("");
  public isScanning = new BehaviorSubject<boolean>(false);
  public cantFindProduce = new BehaviorSubject<boolean>(false);
  private lastKeyEventtimestamp: number

  public subject = new BehaviorSubject<any>([]);

  constructor(private httpClient: HttpClient, private inventoryService: InventoryService) {
    //this.barcode = "";
    this.getBarcodeRules()
    this.getProduces()
    this.isScanning.subscribe( isScanning => {
      if (!isScanning) {
        this.scannedBarcode.next("")
        this.barcode = null
        this.lastKeyEventtimestamp = undefined 
      }
    })
  }

  addKey(key: string, timeStamp) {
    console.log(this.lastKeyEventtimestamp)
    console.log(timeStamp)
    if (timeStamp - this.lastKeyEventtimestamp > 5000) {
      console.log("barcode reset")
      this.isScanning.next(false)
      this.isScanning.next(true)
    }
    console.log("key added under latest version")
    this.isScanning.next(true)
    if (key === ";") {
      console.log("key read")
      this.readBarcode(this.scannedBarcode.getValue())
      this.isScanning.next(false)
    } else {
      console.log("scanned barcode value")
      console.log(this.scannedBarcode.getValue())
      this.lastKeyEventtimestamp = timeStamp
      this.barcode = Number(this.scannedBarcode.getValue().concat(key))
      this.scannedBarcode.next(this.scannedBarcode.getValue().concat(key))
    }      
  }

  addBarcode(barcode: number) {
    this.barcode = barcode
    this.scannedBarcode.next(String(barcode))
    this.readBarcode(String(barcode))
  }

  private readBarcode(barcode: string) {
    Observable.forkJoin(this.barcodeRules,this.produceMap).subscribe( result => {
      let item = this.getItem(barcode, result[0], result[1]).then( item => {
        this.inventoryService.addItem(item)   
      }).catch( error => {
        console.log(error)
      })
    })
  }

  private getBarcodeRules() {
    const url = `${this.inventoryApiUrl}/barcode-rule`;
    this.httpClient.get<any>(url)
    .pipe(
      map(data => data)
      ).subscribe( data => {
        this.barcodeRules.next(data)
        this.barcodeRules.complete()
      });
    }

  private getProduces() {
    let produces = JSON.parse(sessionStorage.getItem('produces'));
    let barcodeRules = JSON.parse(sessionStorage.getItem('barcodeRules'));
    if (produces !== null && produces !== undefined) {
      let produceMap = new Map()
      produces.forEach( produce => {
        produceMap.set(this.trimBarcode(produce.barcode, barcodeRules), produce)
      })
      this.produceMap.next(produceMap)
      this.produceMap.complete()
    } else {
      const url = `${this.inventoryApiUrl}/produce`;
      this.barcodeRules.subscribe( barcodeRules => {
        this.httpClient.get<Produce[]>(url)
        .pipe(
        map(data => data)//,
        //map(<Produce>(produce) => this.trimBarcode(produce.barcode))
        ).subscribe((data) => {
          let produceMap = new Map()
          data.forEach( produce => {
            produceMap.set(this.trimBarcode(produce.barcode, barcodeRules), produce)
          })
          this.produceMap.next(produceMap)
          this.produceMap.complete()
          sessionStorage.setItem('produces', JSON.stringify(data));
          sessionStorage.setItem('barcodeRules', JSON.stringify(barcodeRules));
        })
      })
    }
  }

  private trimBarcode(barcode: string, barcodeRules): string {
    if (barcode !== undefined && barcode !== null) {
      for (let index = 0; index < barcodeRules.length; index++) {
        const barcode_data = barcode.match(barcodeRules[index].expression);
        if (barcode_data && barcode_data[1] && barcode_data[2]) {
          barcode = barcode.substr(0,7)
        }
      }
      //barcode = barcode.replace(/^0+/, '')
    }
    return barcode;
  }

  private round(value: number, decimals: number): number {
    return Math.round(value * Math.pow(10,decimals))/Math.pow(10,decimals)
  } 

  getItem(barcode: string, barcodeRules, produceMap): Promise<Item> {
    let price: number;
    let weight: number;
    let shortBarcode: string
    let produce: Produce
    let that = this
    let func = function (resolve, reject) {
      try{
        while (barcode.length < 13) {
          barcode = '0'.concat(barcode)
        }
        for (let index = 0; index < barcodeRules.length; index++) {
          const barcode_data = barcode.match(barcodeRules[index].expression);
          if (barcode_data && barcode_data[1] && barcode_data[2]) {
            price = Number(barcode_data[1] + '.' + barcode_data[2]);
            shortBarcode = barcode.substr(0,7)

            if (barcodeRules[index].type === 'FP') {
              produce = produceMap.get(shortBarcode)
              weight = price / produce.pricePerUnit / 6.55957
            } else if (barcodeRules[index].type === 'P') {
              produce = produceMap.get(shortBarcode)
              weight = price / produce.pricePerUnit;
            } else if (barcodeRules[index].type === 'W') {
              produce = produceMap.get(shortBarcode)
              weight = price;
            } 
            weight = that.round(weight, 3)
            break;
          }
          else if (barcode_data){
            produce = produceMap.get(barcode)
            weight = 1;
            break;
          }
        }
        console.log("barcode: " + barcode)
        console.log("produce: " + produce)
        if (produce === null || produce === undefined) {
          that.cantFindProduce.next(true)
          reject(new Error('Produit introuvable...'))
        }
        resolve({produce: produce, actualQuantity: weight})
      } catch (error) {
        console.log(error)
        that.cantFindProduce.next(true)
        reject(error)
      }
    }
    return new Promise(func)
  }
}
