import { Component, OnInit } from '@angular/core';

import { AuthService } from '../auth.service';
import { InventoryService } from '../inventory.service';

@Component({
  selector: 'app-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.scss']
})
export class ToolbarComponent implements OnInit {

  isMenuActive = false

  constructor(
    private authService: AuthService,
    private inventoryService: InventoryService) { }

  reset () {
    this.inventoryService.reset().subscribe(res => console.log("inventory reset"))
  }

  logout() {
    this.authService.logout()
  }

  ngOnInit() {
  }

}
