import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { forkJoin } from 'rxjs/observable/forkJoin';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { map, tap } from 'rxjs/operators';
import 'rxjs/add/operator/toPromise';

import { environment } from './../environments/environment';
import { Item } from './item.model';
import { Produce } from './produce.model';
import { Inventory } from './inventory.model';
import { AuthService } from './auth.service';


@Injectable()
export class InventoryService {

  private inventoryApiUrl = environment.inventoryApiUrl;
  item: Item;
  produces: Produce[];
  public items = new BehaviorSubject<Item[]>([]);
  public lastAdded = new BehaviorSubject<Item>({produce: {'id': undefined, 'name': undefined}});
  public inventory = new BehaviorSubject<Inventory>({items:[]});

  constructor(
    private httpClient: HttpClient,
    private authService: AuthService
    // ,@Inject(LOCAL_STORAGE) private storage: StorageService
  ) {
    this.inventory.subscribe((inventory) => {
      sessionStorage.setItem(JSON.stringify(inventory.id), JSON.stringify(inventory));
    })
  }

  getItem(barcode): Observable<Item[] | Item> {
    const url = `${this.inventoryApiUrl}/produce/barcode/` + barcode.toString();
    return this.httpClient.get<any>(url)
      .pipe(
        map(data => data[0]),
        tap(items => this.items.next(items))
      );
  }

  saveItem(item): Observable<Item> {
    const url = `${this.inventoryApiUrl}/item`;
    let body = item
    return this.httpClient.put<any>(url, body)
      .pipe(
        map(data => data)
      );
  }

  updateInventory(inventoryId: number, data: any): Observable<any> {
    return new Observable((obs) => {
      return obs.next(data);
    });
    // const url = `${this.inventoryApiUrl}/inventory?id=${inventoryId}`;
    // return this.httpClient.post<any>(url, { items: data })
    //   .pipe(
    //     map(_data => _data.items)
    //   );
  }

  updateProduceQuantity(produceId, theoreticalQuantity): Observable<any> {
    return new Observable((obs) => {
      return obs.next(theoreticalQuantity);
    });
    // const url = `${this.inventoryApiUrl}/produce?id=${produceId}`;
    // return this.httpClient.post<any>(url, {
    //   theoreticalQuantity
    // })
    //   .pipe(
    //     map(_data => _data)
    //   );
  }

  private round(value: number, decimals: number): number {
    return Math.round(value * Math.pow(10,decimals))/Math.pow(10,decimals)
  }

  addItem(item): void {
      let inventory = this.inventory.getValue()
      const index = inventory.items.findIndex(arrayItem => {
        return arrayItem.produce.id === item.produce.id;
      });
      if (index >= 0) {
        inventory.items[index]['actualQuantity'] = this.round(inventory.items[index]['actualQuantity'] + item['actualQuantity'], 3)
      } else {
        item['theoreticalQuantity'] = this.round(item['produce'].quantityInStock, 3)
        inventory.items = inventory.items.concat([item]);
        let indexLast = inventory.items.length - 1
        item['location'] = inventory.location
        item['inventory'] = inventory.id
        this.saveItem(item).subscribe( item => {
          inventory.items[indexLast]['id'] = item['id']
          this.inventory.next(inventory)
        })
      }
      this.lastAdded.next(item);
      this.inventory.next(inventory);
  }

  getInventories(): Observable<Inventory> {
    const url = `${this.inventoryApiUrl}/inventory`;
    return this.httpClient.get<any>(url)
      .pipe(
        map(data => data)
      );
  }

  getProduces(): Observable<Produce> {
    const url = `${this.inventoryApiUrl}/inventory`;
    return this.httpClient.get<any>(url)
      .pipe(
        map(data => data),
        tap(data => {
          let produceMap = new Map();
          data.forEach( produce => {
            produceMap.set(produce.barcode,produce)
          })
          sessionStorage.setItem('produces', JSON.stringify(produceMap));
        })
      );
  }

  getInventoriesByStatus(status): Observable<Inventory> {
    const url = `${this.inventoryApiUrl}/inventory/state/${status}`;
    return this.httpClient.get<any>(url)
      .pipe(
        map(data => data.inventories)
      );
  }

  getInventory(inventoryId): Observable<Inventory> {
    let inventory = JSON.parse(sessionStorage.getItem(inventoryId));
    if (inventory !== null && inventory !== undefined) {
      console.log("inventory found")
      return new Observable((obs) => {
        this.inventory.next(inventory)
        obs.next(inventory);
      })
    } else {
    console.log("inventory NOT found")
    const url = `${this.inventoryApiUrl}/inventory/` + inventoryId;
    return this.httpClient.get<any>(url)
      .pipe(
        map(data => data),
        tap(data =>  {
          this.inventory.next(data)
        })
      );
     }
  }

  storeInLocalStorage(items): Observable<any> {
    return new Observable((obs) => {
      sessionStorage.setItem('items', JSON.stringify(items));
      return obs.next({
        data: true
      });
    });
  }

  getFromLocalStorage(): Observable<any> {
    return new Observable((obs) => {
      return obs.next({
        data: JSON.parse(sessionStorage.getItem('items'))
      });
    });
  }

  saveLocal(inventory): void {
    sessionStorage.setItem(JSON.stringify(inventory.id), JSON.stringify(inventory));
  }

  save(inventory): Observable<Inventory> {
    //this.inventory.next(inventory);
    let body = inventory
    const url = `${this.inventoryApiUrl}/inventory/` + inventory.id;
    return this.httpClient.post<any>(url, body)
      .map(data => data)
      .catch(this.handleErrorObservable)
  }

  reset(): Observable<Inventory> {
    let inventoryId = this.inventory.getValue().id
    this.inventory.next({items:[]})
    sessionStorage.removeItem(JSON.stringify(inventoryId));
    const url = `${this.inventoryApiUrl}/inventory/` + inventoryId;
    return this.httpClient.get<any>(url)
      .pipe(
        map(data => data),
        tap(data =>  {
          // data.items.forEach( item => {
          //   item.actualQuantity = 0
          // })
          this.inventory.next(data)
        })
      );
  }

  private handleErrorObservable (error: Response | any) {
    console.error(error.message || error);
    return Observable.throw(error.message || error);
  }
}
