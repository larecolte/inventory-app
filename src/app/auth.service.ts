import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { catchError, map, concatMap, tap, filter, flatMap } from 'rxjs/operators';
import { pipe } from 'rxjs/Rx';
import { Router } from '@angular/router';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { JwtHelperService  } from '@auth0/angular-jwt';

import { environment } from './../environments/environment';

const helper = new JwtHelperService();

@Injectable()
export class AuthService {

  private loginUrl = environment.loginUrl;
  public token = new BehaviorSubject<string>("Bearer");

  constructor(
    private httpClient: HttpClient,
    public router: Router ) { }


  isAuthenticated(): boolean {
    const token = localStorage.getItem('token');
    //console.log(helper.isTokenExpired(token))
    if (token === undefined || token === null) {
      return false
    } else {
      this.token.next(token)
      return true
    }
  }


  login(user): Observable<string> {
    const url = `${this.loginUrl}/login`;
    return this.httpClient.post<any>(url, user)
      .pipe(
        map(data => data.token),
        tap(token => {
          this.token.next(token);
          localStorage.setItem('token', token)
          this.router.navigate(['inventories']);
        }),
        pipe(
          catchError(this.handleError)
        )//,
       );
  }

  logout(): void {
    localStorage.removeItem('token')
    this.router.navigate(['login']);
  }

  private handleError(error: HttpErrorResponse) {
  if (error.error instanceof ErrorEvent) {
    // A client-side or network error occurred. Handle it accordingly.
    console.error('An error occurred:', error.error.message);
  } else {
    // The backend returned an unsuccessful response code.
    // The response body may contain clues as to what went wrong,
    console.error(
      `Backend returned code ${error.status}`);
  }
  // return an observable with a user-facing error message
  return Observable.throw(
    'Unauthorized');
};

}
