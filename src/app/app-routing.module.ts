import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component'
import { InventoriesComponent } from './inventories/inventories.component'
import { ItemListComponent } from './item-list/item-list.component'
import { AuthGuardService } from './auth-guard.service';

const routes: Routes = [
  { path: '', component: InventoriesComponent, canActivate: [AuthGuardService] },
  { path: 'login',  component: LoginComponent },
  { path: 'inventories', component: InventoriesComponent, canActivate: [AuthGuardService] },
  { path: 'inventory/:inventory', component: ItemListComponent, canActivate: [AuthGuardService] }];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
