import { TestBed, inject, async } from '@angular/core/testing';
import { HttpModule }    from '@angular/http';
import { HttpClientModule }    from '@angular/common/http';

import { InventoryService } from './inventory.service';
import { Item } from './item.model';
import { LOCAL_STORAGE, StorageService } from 'ngx-webstorage-service';

describe('InventoryService', () => {

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpModule,
        HttpClientModule
      ],
      providers: [InventoryService, StorageService]
    });
  });

  it('should be created', inject([InventoryService], (service: InventoryService) => {
    expect(service).toBeTruthy();
  }));


  //Should expose a method to get a item from its barcode. The method should call the REST API at the following address: /produce/barcode/:barcode
  it('should get a product from a barcode (via REST API call)', async(inject([InventoryService], (service: InventoryService) => {
    let expectedResult = {"id":2,"barcode": 3770010589328,"name":"Kombucha Citron","theoreticalQuantity":2,"pricePerUnit":5.5}
    service.getItem(3770010589328).subscribe((item) => {
      expect(item).toContain(expectedResult);
     })
  })));

  //Should expose a method to get an inventory list. The method should call the REST API at the following address: /inventory
  it('should get an inventory list', async(inject([InventoryService], (service: InventoryService) => {
    let expectedResult = {
      "id":2,
      "name":"Kitchen Inventory",
      "items":
        [{"id":1,"barcode": 2023092000008,"name":"Daily Special","theoreticalQuantity":5,"actualQuantity":0}, 
        {"id":2,"barcode": 2223093000001,"name":"Home Made Foie Gras","theoreticalQuantity":15,"actualQuantity":0},
        {"id":3,"barcode": 2022570025588,"name":"Dessert","theoreticalQuantity":2,"actualQuantity":0}],
      "warehouse": {"id": 1},
      "state": "In Progess"
      }
    service.getInventories().subscribe((inventories) => {
      expect(inventories).toContain(expectedResult);
     })
  })));

  //Should expose a method to start an inventory. Starting the inventory will set service.items equal to the items of the selected inventory
  it('should get an inventory list', async(inject([InventoryService], (service: InventoryService) => {
    let inventoryId = 2
    let expectedResult = [{"id":1,"barcode": 2023092000008,"name":"Daily Special","theoreticalQuantity":5,"actualQuantity":0},
    {"id":2,"barcode": 2223093000001,"name":"Home Made Foie Gras","theoreticalQuantity":15,"actualQuantity":0},
    {"id":3,"barcode": 2022570025588,"name":"Dessert","theoreticalQuantity":2,"actualQuantity":0}]
    service.startInventory(inventoryId)
    service.items.subscribe((items) => {
      expect(items).toEqual(expectedResult);
     })
  })));


  //Should expose a method to add an item to the item list by providing a barcode.
  //If the item is already in the list, the quantity is incremented by the item quantity.
  //If the item is not in the list, it should be added by calling the REST API at the following address: /produce/barcode/:barcode
  //If we are offline, it should return an error saying that we can't add product while we are offline
  it('should increment quantity or add item to items', async(inject([InventoryService], (service: InventoryService) => {
    service.addItem(3770010589328).subscribe((item) => {
      service.addItem(3770010589327).subscribe((item) => {
        service.addItem(3770010589327).subscribe((item) => {
          service.items.subscribe((items) => {
            expect(items.length).toEqual(2);
          })
        })
      })
     })
  })));

  //items should be a BehaviorSubject on the inventory service. Component can subscribe to it to get the items of the current inventory.
  it('should return a list of items', async(inject([InventoryService], (service: InventoryService) => {
    service.items.subscribe((result) => {
      expect(result).toEqual(jasmine.any(Array));
     })
  })));

  //items should be save locally to make the app work offline
  it('the list of items should be save locally', async(inject([InventoryService], (service: InventoryService, @Inject(LOCAL_STORAGE) storageService: StorageService) => {
    expect(storageService.get('items')).toEqual(jasmine.any(Array));
  })));

});
